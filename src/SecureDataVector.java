import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

class SecureDataVector <E extends Serializable> implements SecureDataContainer <E> {
	// AF: { <Id_0, passw_0, { data_0_0, ... ,  data_0_m }>, ... , <Id_n-1, passw_n-1, { data_n-1_0, ... ,  data_n-1_k }> }

	//IR :  secureData != null && secureData.size() >= 0 &&
	//			forall i . 0 <= i < secureData.size()  ==> secureData.get(i) != null 
	//			forall i != j. 0 <= i, j < secureData.size() ==> secureData.get(i).getId() != secureData.get(j).getId()

	private Vector < UserData <E>> secureData;
	public SecureDataVector() {
		this.secureData = new Vector <> ();
	}

	@Override
	public void createUser(String Id, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Id == null)  throw new NullPointerException("Id is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		for (UserData <E> user: this.secureData) {
			if (user.getId().equals(Id))
				throw new IllegalArgumentException("User with id " + Id + " already in the collection");
		}
		UserData <E> user = new UserData <E> (Id, passw);
		this.secureData.add(user);
	}

	@Override
	public void removeUser(String Id, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Id == null)  throw new NullPointerException("Id is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Id, passw)) {
				this.secureData.remove(user);
				return;
			}
			if(user.getId().equals(Id))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Id + " is not in the collection");
	}

	@Override
	public int getSize(String Owner, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				return user.dataSize();
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

	@Override
	public boolean put(String Owner, String passw, E data) throws NullPointerException, 
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				return user.add(data);
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

	@Override
	public E get(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				return user.get(data);
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

	@Override
	public E remove(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				return user.remove(data);
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

	@Override
	public void copy(String Owner, String passw, E data) throws NullPointerException, 
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				E tmp = user.get(data);
				if (tmp != null) {
					user.add(tmp);
					return;
				} else {
					throw new IllegalArgumentException("Data is not in the collection");
				}
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

	@Override
	public void share(String Owner, String passw, String Other, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				E currentData = user.get(data);
				if(currentData == null) {
					throw new IllegalArgumentException("Data is not in the container");
				} else {
					for (UserData <E> otherUser: this.secureData) {
						if (otherUser.getId().equals(Other)) {
							otherUser.add(currentData);
							return;
						}
					}
					throw new IllegalArgumentException("User with id " + Other + " is not in the collection");
				}
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

	@Override
	public Iterator <E> getIterator(String Owner, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		for (UserData <E> user: this.secureData) {
			if (user.check(Owner, passw)) {
				return new ReadOnlyIterator<>(user.getIterator());
			}
			if(user.getId().equals(Owner))
				throw new IllegalArgumentException("Wrong password");
		}
		throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
	}

}
import java.util.HashMap;
import java.util.Iterator;

class SecureDataMap <E> implements SecureDataContainer <E> {
	// AF = {Id_0, ..., Id_n} tale che f(Id_k) -> <password_k, {data_0, ..., data_m}>

	//IR = map != null && map.size() >= 0 &&
	//			forall i. i in Keys ==> map.get(i) != null

	private final HashMap < String,
	SecureValue <E>> map;

	public SecureDataMap() {
		this.map = new HashMap<>();
	}

	@Override
	public void createUser(String Id, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Id == null)  throw new NullPointerException("Id is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if (this.map.containsKey(Id))
			throw new IllegalArgumentException("User with id " + Id + " already in the collection");
		SecureValue <E> value = new SecureValue<>(passw);
		this.map.put(Id, value);
	}

	@Override
	public void removeUser(String Id, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Id == null)  throw new NullPointerException("Id is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		SecureValue <E> tmp = this.map.get(Id);
		if(tmp == null)	
			throw new IllegalArgumentException("User with id " + Id + " is not in the collection");
		if(!tmp.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		this.map.remove(Id);
	}

	@Override
	public int getSize(String Owner, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		SecureValue <E> tmp = this.map.get(Owner);
		if(tmp == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(!tmp.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		return tmp.size();
	}

	@Override
	public boolean put(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		SecureValue <E> tmp = this.map.get(Owner);
		if(tmp == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(!tmp.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		return tmp.add(data);
	}

	@Override
	public E get(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		SecureValue <E> tmp = this.map.get(Owner);
		if(tmp == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(!tmp.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		return tmp.get(data);
	}

	@Override
	public E remove(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		SecureValue <E> tmp = this.map.get(Owner);
		if(tmp == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(!tmp.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		return tmp.remove(data);
	}

	@Override
	public void copy(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		SecureValue <E> tmp = this.map.get(Owner);
		if(tmp == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(!tmp.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		E value = tmp.get(data);
		if (value != null) tmp.add(value);
		else throw new IllegalArgumentException("Data is not in the collection");
	}

	@Override
	public void share(String Owner, String passw, String Other, E data) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		if(data == null)  throw new NullPointerException("Data is null");
		SecureValue <E> first = this.map.get(Owner);
		SecureValue <E> second = this.map.get(Other);
		if(first == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(second == null)	
			throw new IllegalArgumentException("User with id " + Other + " is not in the collection");
		if(!first.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		E currentData = first.get(data);
		if (currentData != null) second.add(currentData);
		else throw new IllegalArgumentException("Data is not in the collection");
	}

	@Override
	public Iterator <E> getIterator(String Owner, String passw) throws NullPointerException,
	IllegalArgumentException {
		if(Owner == null)  throw new NullPointerException("Owner is null");
		if(passw == null)  throw new NullPointerException("Password is null");
		SecureValue <E> value = this.map.get(Owner);
		if(value == null)	
			throw new IllegalArgumentException("User with id " + Owner + " is not in the collection");
		if(!value.checkPassword(passw))
			throw new IllegalArgumentException("Wrong password");
		return value.getIterator();
	}

}
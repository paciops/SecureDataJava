import java.util.Iterator;

public interface SecureDataContainer < E > {
	/*OVERVIEW: Tipo modificabile che gestisce dati generici di utenti
	 * 					autenticati tramite id e password
	 * TIPICAL ELEMENT: 
	 *					{ <Id_0, passw_0, { data_0_0, ... ,  data_0_m }>, ... , <Id_n-1, passw_n-1, { data_n-1_0, ... ,  data_n-1_k }> }
	 *					dove Id_i != Id_j per ogni i != j e 0 <= i < n e 0 <= j < n
	 *					dove data_g_h è il dato di indice h e dell'utente con Id_g
	 *					dove n è il numero attuali di utenti presenti nel contenitore
	 *					inoltre la collezione di dati generici può essere anche vuota ma non può contenere valori nulli
	 */

	// Crea l’identità un nuovo utente della collezione
	public void createUser(String Id, String passw) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Id != null && passw != null
	 * THROWS: se Id == null || passw == null lancia NullPointerException
	 * THROWS: se è già presente un utente con lo stesso Id lancia IllegalArgumentException
	 * MODIFIES: this
	 * EFFECTS:  aggiunge l'Id alla collezione se non è presente
	 */

	// Rimuove l’utente dalla collezione
	public void removeUser(String Id, String passw) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Id != null && passw != null
	 * THROWS: se Id == null || passw == null lancia NullPointerException
	 * THROWS: se è già presente un utente con lo stesso Id lancia IllegalArgumentException
	 * THROWS: se è già presente un utente ma la password è sbagliata lancia IllegalArgumentException
	 * MODIFIES: this
	 * EFFECTS: rimuove l'utente con l'id passato se presente dopo aver controllato 
	 * 					se la password è giusta
	 */

	// Restituisce il numero degli elementi di un utente presenti nella
	// collezione
	public int getSize(String Owner, String passw) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Owner != null && passw != null
	 * THROWS: se Owner == null || passw == null lancia NullPointerException
	 * THROWS: se non è presente l'utente passato lancia IllegalArgumentException
	 * THROWS: se l'utente è presente ma la password è sbagliata lancia IllegalArgumentException
	 * EFFECTS:  restituisce il numero degli elementi di un utente presenti nella collezione
	 */

	// Inserisce il valore del dato nella collezione
	// se vengono rispettati i controlli di identità
	public boolean put(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Owner != null && passw != null && data != null
	 * THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	 * THROWS: se non è presente l'utente passato lancia IllegalArgumentException
	 * THROWS: se l'utente è presente ma la password è sbagliata lancia IllegalArgumentException
	 * MODIFIES: this
	 * EFFECTS:  ritorna true se l'elemento viene inserito nella collezione
	 */

	// Ottiene una copia del valore del dato nella collezione
	// se vengono rispettati i controlli di identità
	public E get(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Owner != null && passw != null && data != null
	 * THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	 * THROWS: se non è presente l'utente passato lancia IllegalArgumentException
	 * THROWS: se l'utente è presente ma la password è sbagliata lancia IllegalArgumentException
	 * EFFECTS:  ritorna la copia presente nella collezione del dato passato se il dato è presente
	 * 					altrimenti se non è presente ritorna null
	 */

	// Rimuove il dato nella collezione
	// se vengono rispettati i controlli di identità
	public E remove(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Owner != null && passw != null && data != null
	 * THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	 * THROWS: se non è presente l'utente passato lancia IllegalArgumentException
	 * THROWS: se l'utente è presente ma la password è sbagliata lancia IllegalArgumentException
	 * MODIFIES: this
	 * EFFECTS: rimuove data dalla collezione e lo ritorna se presente, altrimenti ritorna null
	 */

	// Crea una copia del dato nella collezione
	// se vengono rispettati i controlli di identità
	public void copy(String Owner, String passw, E data) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Owner != null && passw != null && data != null
	 * THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	 * THROWS: se non è presente l'utente passato lancia IllegalArgumentException
	 * THROWS: se l'utente è presente ma la password è sbagliata lancia IllegalArgumentException
	 * THROWS: se il dato non è presente nella collezione lancia IllegalArgumentException
	 * MODIFIES: this
	 * EFFECTS:  crea una copia del dato nella collezione se presente
	 */

	// Condivide il dato nella collezione con un altro utente
	// se vengono rispettati i controlli di identità
	public void share(String Owner, String passw, String Other, E data) throws NullPointerException,
	IllegalArgumentException;
	/*
	* REQUIRES: Owner != null && passw != null && Other != null && data != null
	* THROWS: se Owner == null || passw == null || Other == null || data == null lancia NullPointerException
	* THROWS: se il dato non è presente nella collezione lancia IllegalArgumentException
	*	THROWS: se non è presente (Owner) lancia IllegalArgumentException
	* THROWS: se l'utente (Owner) è presente ma la password è sbagliata lancia IllegalArgumentException
	* THROWS: se l'utente (Other) non è presente lancia IllegalArgumentException
	* MODIFIES: this
	* EFFECTS:  aggiunge il dato presente nella collezione originale alla collezione di dati di Other 
							se data appartiene alla collezione di dati di Owner
	*/

	// restituisce un iteratore (senza remove) che genera tutti i dati
	// dell’utente in ordine arbitrario
	// se vengono rispettati i controlli di identità
	public Iterator < E > getIterator(String Owner, String passw) throws NullPointerException,
	IllegalArgumentException;
	/*
	 * REQUIRES: Owner != null && passw != null
	 * THROWS: se Owner == null || passw == null lancia NullPointerException
	 * THROWS: se non è presente l'utente passato lancia IllegalArgumentException
	 * THROWS: se l'utente è presente ma la password è sbagliata lancia IllegalArgumentException
	 * EFFECTS:  ritorna un iteratore senza il metodo remove contentente tutti i dati dell'utente
	 */
}
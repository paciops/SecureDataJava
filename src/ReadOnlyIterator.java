import java.util.Iterator;

class ReadOnlyIterator <E> implements Iterator <E> {
	private Iterator <E> base;

	public ReadOnlyIterator(final Iterator <E> it) {
		if (it == null) {
			throw new NullPointerException("Base iterator is null.");
		}
		this.base = it;
	}

	@Override
	public E next() {
		return this.base.next();
	}

	@Override
	public boolean hasNext() {
		return this.base.hasNext();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Remove is unsupported on ReadOnlyIterator");
	}
}
import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;
import java.util.Iterator;

class Main {
  public static void stampa(Object obj) {
    System.out.println(obj);
  }
  public static void main(String[] args) {
    SecureDataMap < Point > pointMap = new SecureDataMap < > ();
    SecureDataVector < Color > colorVector = new SecureDataVector < > ();
    try {
      pointMap.createUser("1", "abcd");
      colorVector.createUser("1", "abcd");
      try {
        stampa("1 dovrebbe essere già presente");
        pointMap.createUser("1", "abcd");
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        stampa("2 non dovrebbe essere già presente");
        pointMap.removeUser("2", "aaa");
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        stampa("1 dovrebbe essere già presente");
        colorVector.createUser("1", "abcd");
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        stampa("2 non dovrebbe essere già presente");
        colorVector.removeUser("2", "aaa");
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        stampa("Dovrebbe dire che il dato non è presente nella collezione");
        pointMap.copy("1", "abcd", new Point(3, 5));
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        stampa("Dovrebbe dire che il dato non è presente nella collezione");
        colorVector.copy("1", "abcd", new Color(345));
      } catch (Exception e) {
        e.printStackTrace();
      }
      Point uno = new Point(1, 1);
      pointMap.put("1", "abcd", uno);
      try {
        stampa("Dovrebbe dire che la password è sbagliata");
        pointMap.get("1", "", uno);
      } catch (Exception e) {
        e.printStackTrace();
      }
      pointMap.put("1", "abcd", new Point(23, 45));
      pointMap.put("1", "abcd", new Point(42, 66));
      pointMap.put("1", "abcd", new Point(0, 99));
      colorVector.put("1", "abcd", new Color(847));
      colorVector.put("1", "abcd", new Color(000));
      colorVector.put("1", "abcd", new Color(111));
      colorVector.put("1", "abcd", new Color(123));
      colorVector.put("1", "abcd", new Color(03));
      colorVector.put("1", "abcd", new Color(42));
      colorVector.copy("1", "abcd", new Color(42));
      stampa("Dovrebbe avere 4 elementi: " + (pointMap.getSize("1", "abcd") == 4));
      pointMap.copy("1", "abcd", uno);
      stampa("Dovrebbe avere 5 elementi: " + (pointMap.getSize("1", "abcd") == 5));
      stampa("Dovrebbe avere 7 elementi: " + (colorVector.getSize("1", "abcd") == 7));
      Iterator < Point > itPoint = pointMap.getIterator("1", "abcd");
      stampa("Punti dell'utente 1");
      while (itPoint.hasNext()) {
        stampa(itPoint.next());
      }
      pointMap.createUser("2", "prova");
      pointMap.share("1", "abcd", "2", new Point(23, 45));
      itPoint = pointMap.getIterator("2", "prova");
      stampa("Punti dell'utente 2");
      while (itPoint.hasNext()) {
        stampa(itPoint.next());
      }
      stampa(pointMap.get("1", "abcd", uno));
      stampa(pointMap.get("2", "prova", new Point(23, 45)));
      Iterator < Color > itColor = colorVector.getIterator("1", "abcd");
      stampa("Colori dell'utente 1");
      while (itColor.hasNext()) {
        stampa(itColor.next());
      }

      // Test con webpage
      SecureDataVector < WebPage > internet = new SecureDataVector < > ();
      internet.createUser("A", "a");
      internet.createUser("B", "bb");
      WebPage google = new WebPage("google home", "google web page");
      internet.put("B", "bb", google);
      google.content = new String("Ho cambiato il contenuto della pagina");
      stampa(internet.get("B", "bb", google));
      internet.copy("B", "bb", google);
      internet.put("B", "bb", google);
      internet.share("B", "bb", "A",google);
      Iterator < WebPage > webItm = internet.getIterator("B", "bb");
      stampa("Iteratore webpage di 2");
      while (webItm.hasNext())
        stampa(webItm.next());
    } catch (Exception e) {
      stampa("Qui non dovrei mai arrivarci");
      e.printStackTrace();
    }
  }
}

class WebPage implements Serializable{
  private static final long serialVersionUID = 42;
  public String title;
  public String content;
  public WebPage(String t, String c) {
    title = t;
    content = c;
  }
  public WebPage() {
    title = new String();
    content = new String();
  }
  
  @Override
  public boolean equals(Object o) {
    if (o == this)  return true;
    if (!(o instanceof WebPage))  return false;
    WebPage page = (WebPage) o;
    return title.equals(page.title);
  }
}
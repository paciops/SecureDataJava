import java.util.ArrayList;
import java.util.Iterator;

class SecureValue <E> {
	// AF = <password, { data_0, ... , data_n }>
	//		password è la password dell'utente
	//		data è un array di generici

	//IR = password != null data != null && data.size >= 0 &&
	//		forall i. 0 <= i < data.size() => data.get(i) != null
	private final String password;
	private final ArrayList <E> data;

	public SecureValue(String passw) {
		this.password = passw;
		this.data = new ArrayList <E> ();
	}

	public boolean checkPassword(String passw) {
		return this.password.equals(passw);
	}

	public boolean add(E elem) {
		return this.data.add(elem);
	}

	public int size() {
		return this.data.size();
	}

	public E remove(E elem) {
		int index = this.data.indexOf(elem);
		if (index > -1) return this.data.remove(index);
		else return null;
	}

	public boolean contains(E elem) {
		return this.data.contains(elem);
	}

	public Iterator <E> getIterator() {
		return new ReadOnlyIterator < > (this.data.iterator());
	}

	public E get(E elem) {
		int index = this.data.indexOf(elem);
		if (index > -1) return this.data.get(index);
		else return null;
	}
}
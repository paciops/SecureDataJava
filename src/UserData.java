import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class UserData <E> {
	// AF = < user, password, data[], ivspec> dove
	// 		user è l'Id dell'utente
	// 		password è la password dell'utente
	// 		data[] è il vettore contenente l'oggetto cryptato 
	//			(il vettore può contenere elementi nulli nel caso
	//			ci siano errori nella fase di codifica, decodifica o
	//			trasformazione dell'oggetto in un array di byte e viceversa)
	//		ivspec è il vettore di inzializzazione del cifrario
	//		transformation è la tipologia di algoritmo scelto per la crittografia

	// IR = user != null  && password != null && data.size >= 0

	private final String user;
	private final String password;
	private final Vector <byte[]> data;
	private final String transformation = "AES/CBC/PKCS5Padding";
	private final IvParameterSpec ivspec;

	public UserData(String Id, String pass) {
		this.user = Id;
		this.password = pass;
		this.data = new Vector <> ();
		this.ivspec = initIV();
	}

	public boolean check(String Id, String pass) {
		return this.user.equals(Id) && this.password.equals(pass);
	}

	public String getId() {
		return this.user;
	}

	public int dataSize() {
		return this.data.size();
	}

	public boolean add(E elem) {
		return this.data.add(encrypt(elem));
	}

	public E remove(E elem) {
		E currentElem = this.get(elem);
		if(currentElem != null) {
			byte[] encrypted = encrypt(currentElem);
			this.data.remove(encrypted);
		}
		return currentElem;
	}

	public E get(E elem) {
		for(byte[] current: this.data) {
			E decrypted = decrypt(current);
			if(elem.equals(decrypted)) {
				return decrypted;
			}
		}
		return null;
	}

	public boolean contains(E elem) {
		return this.get(elem) != null;
	}

	public Iterator <E> getIterator() {
		Vector<E> result = new Vector<>();
		for(byte[] current: this.data) {
			E decrypted = decrypt(current);
			result.add(decrypted);
		}
		return result.iterator();
	}

	//Cripta il generico elem passato
	private byte[] encrypt(E elem) {
		try {
			//Genera la chiave simmetrica tramite l'hash della password dell'utente
			SecretKeySpec key = new SecretKeySpec(passwordHash(), "AES");
			//Crea il cifrario tramite l'algoritmo speficcato all'inizio della classe
			Cipher ci = Cipher.getInstance(this.transformation);
			//Lo inizializza tramite la chiave ed il vettore di inizializzazione
			ci.init(Cipher.ENCRYPT_MODE, key, this.ivspec);
			//Dopodichè trasformo la classe in un array di byte
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			ObjectOutputStream objStream = new ObjectOutputStream(byteStream);
			objStream.writeObject(elem);
			//Cifro la classe trasformata in array di byte e la ritorno al chiamante
			return ci.doFinal(byteStream.toByteArray());	
		} catch (Exception e) {
			//Nel caso la procedura fallisse ritorno null
			e.printStackTrace();
			return null;
		}
	}

	//Decripta l'array di byte passato
	@SuppressWarnings("unchecked")
	private E decrypt(byte[] value) {
		try {
			//Genera la chiave simmetrica tramite l'hash della password dell'utente
			SecretKeySpec key = new SecretKeySpec(passwordHash(), "AES");
			//Crea il cifrario tramite l'algoritmo speficcato all'inizio della classe
			Cipher cipher = Cipher.getInstance(transformation);
			//Lo inizializza tramite la chiave ed il vettore di inizializzazione
			cipher.init( Cipher.DECRYPT_MODE, key, this.ivspec);
			//Dopodichè decripto l'array di byte passato in un array di byte
			ByteArrayInputStream byteStream = new ByteArrayInputStream(cipher.doFinal(value));
			ObjectInputStream objStream = new ObjectInputStream(byteStream);
			//Ritorno l'array di byte trasformato in Object e castato al generico
			return (E) objStream.readObject();	
		} catch (Exception e) {
			//Nel caso la procedura fallisse ritorno null
			e.printStackTrace();
			return null;
		}
	}

	// Ritorna l'hash della password 
	private byte[] passwordHash() throws Exception{
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		return digest.digest(password.getBytes(StandardCharsets.UTF_8));
	}

	// Genera il vettore di inizializzazione. Vedi anche https://www.novixys.com/blog/java-aes-example/#3_Generate_an_Initialization_Vector_IV
	private IvParameterSpec initIV() {
		// Generating IV.
		int ivSize = 16;
		byte[] iv = new byte[ivSize];
		SecureRandom random = new SecureRandom();
		random.nextBytes(iv);
		return new IvParameterSpec(iv);
	}
}